function start() {
    /* initialise timer display */
    timerElement.innerHTML = timer.time();

    startTimer();
}

var Timer = function() {
    this.minutes = 0;
    this.seconds = 0;
    this.hours = 0;
};

Timer.prototype.time = function() {
    var output = "";
    if (this.minutes < 10) {
        output += "0";
    }
    output += this.minutes.toString();
    output += ":";
    if (this.seconds < 10) {
        output += "0";
    }
    output += this.seconds.toString();
    return output;
};

Timer.prototype.increment = function() {
    this.seconds++;
    if (this.seconds > 59) {
        this.seconds = 0;
        this.minutes++;
    }
    if (this.minutes > 59) {
        this.hours++;
    }
};

var timer = new Timer();
var timerElement = document.getElementById("timer");

var interval;

function startTimer() {
    interval = setInterval(incrementTimer, 1000);
    document.getElementById("start").disabled = true;
    document.getElementById("stop").disabled = false;
}

function stopTimer() {
    clearInterval(interval);
    document.getElementById("start").disabled = false;
    document.getElementById("stop").disabled = true;
}

function incrementTimer() {
    timer.increment();
    timerElement.innerHTML = timer.time();
}

